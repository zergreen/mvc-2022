const express = require("express");
const router = express.Router();

const connection = require("../database.connect/connector");

const { Endpoint } = require("../controller/endpoint.js");

router.get("/", function (req, res) {
  res.redirect("getsortlocation");
});

router.get("/edit/:id", new Endpoint().updateLocationEndpoint);

// // get all data from table Location
router.get("/getalllocation", new Endpoint().getAllLocationEndpoint);

// // create data from table Location
router.post("/createlocation", new Endpoint().createLocationEndpoint);

router.post("/deletelocationby/:id", new Endpoint().deleteLocationByIDEndpoint);

router.put("/updatelocation", new Endpoint().updateLocationEndpoint);

router.get("/getsortlocation", new Endpoint().getSortLocationEndpoint);

module.exports = router;
