const connection = require("../database.connect/connector");
class Plugin {
  getAllLocationPlugin = (req, res) => {
    let sql = `SELECT * FROM Location`;
    connection.query(sql, function (err, result) {
      if (err) {
        console.log("error to query");
        console.log(err);
      } else {
        console.table(result);

        return res.status(200).send({ response: result });
      }
    });
  };

  getSortLocationPlugin = (location, res) => {
    let sql = `SELECT * FROM Location ORDER BY score DESC`;
    connection.query(sql, function (err, result) {
      if (err) {
        console.log("error to query");
        console.log(err);
      } else {
        console.log("sort finished");
        console.table(result);
        // return res.status(200).send({ response: result });
        return res.render("index.ejs", {
          response: result,
          title: "MVC 63050159",
        });
      }
    });
  };

  createLocationPlugin = (location, res) => {
    console.table(location);
    let sql = `INSERT into Location 
    values 
      (
        ?, ?, ?, ?, ?, ?, ?
      )    
    `;
    connection.query(
      sql,
      [
        location.id,
        location.name,
        location.lat,
        location.lat_mer,
        location.lon,
        location.lon_mer,
        location.score,
      ],
      function (err, result) {
        if (err) {
          console.log("error to query");
          console.log(err);
        } else {
          console.log("query finished");
          return res
            .status(201)
            .send({ response: "create Location Finished!" });
        }
      }
    );
  };

  deleteLocationByIDPlugin = (id, res) => {
    let sql = `DELETE FROM Location WHERE id = ?`;
    connection.query(sql, [id.id], function (err, result) {
      if (err) {
        console.log("error to query");
        console.log(err);
      } else {
        console.log("you come here");
        console.table(result);
        return res.status(201).send({ response: "delete with id Finished" });
        // return res.redirect("/");
      }
    });
  };

  updateLocationPlugin = (location, res) => {
    let sql = `UPDATE Location 
  SET 
    name = ?, 
    lat = ?, 
    lat_mer = ?, 
    lon = ?, 
    lon_mer = ? 
  WHERE 
    id = ?;    
    `;
    connection.query(sql, [
      location.name,
      location.lat,
      location.lat_mer,
      location.lon,
      location.lon_mer,
    ]),
      function (err) {
        if (err) {
          console.log("error to query");
        } else {
          return res.status(203).send({ response: "update finished" });
        }
      };
  };
}

module.exports = { Plugin };
