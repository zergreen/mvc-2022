const { Plugin } = require("../controller/plugin");
class Logic {

  getAllLocationLogic = (req, res) => {
    new Plugin().getAllLocationPlugin(req, res);
  };

  createLocationLogic = (location, res) => {
    new Plugin().createLocationPlugin(location, res);
  };

  deleteLocationByIDLogic = (id, res) => {
    new Plugin().deleteLocationByIDPlugin(id, res);
  };

  updateLocationLogic = (location, res) => {
    new Plugin().updateLocationPlugin(location, res);
  };

  getSortLocationLogic = (req, res) => {
    new Plugin().getSortLocationPlugin(req, res);
  };
}
module.exports = { Logic };
