const { Logic } = require("./logic");
const models = require("../model/model");

class Endpoint {
  constructor() {
    this.location = models.locationModel;
  }

  getAllLocationEndpoint = (req, res) => {
    new Logic().getAllLocationLogic(req, res);
  };

  createLocationEndpoint = (req, res) => {
    this.location.id = null;
    this.location.name = req.body.name;
    this.location.lat = req.body.lat;
    this.location.lat_mer = req.body.lat_mer;
    this.location.lon = req.body.lon;
    this.location.lon_mer = req.body.lon_mer;
    this.location.score = 0;
    new Logic().createLocationLogic(this.location, res);
  };

  deleteLocationByIDEndpoint = (req, res) => {
    var id = req.params.id;
    new Logic().deleteLocationByIDLogic(id, res);
  };

  updateLocationEndpoint = (req, res) => {
    this.location.id = id;
    this.location.name = req.body.name;
    this.location.lat = req.body.lat;
    this.location.lat_mer = req.body.lat_mer;
    this.location.lon = req.body.lon;
    this.location.lon_mer = req.body.lon_mer;
    new Logic().updateLocationLogic(this.location, res);
  };

  getSortLocationEndpoint = (req, res) => {
    new Logic().getSortLocationLogic(req, res);
  };
}
module.exports = {
  Endpoint,
};
