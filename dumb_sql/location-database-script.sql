CREATE TABLE Location (
  id int auto_increment primary key, 
  name varchar(255), 
  lat double, 
  lat_mer char, 
  lon double, 
  lon_mer char, 
  score double
);

-- อันนี้ไม่เกี่ยว ให้ดูเฉยๆ ครับ
SELECT * FROM cputable;

INSERT into cputable VALUES ( null, 70, 10)

SELECT * FROM Location;

insert into Location values (null, "LDN", 51.51, 'N', 0.13, 'W', 789);

UPDATE Location
SET name = ?, lat = ?, lat_mer = ?, lon = ?, lon_mer = ?
WHERE id = ?;

SELECT id, name, lat, lat_mer, lon, lon_mer, score
FROM Location
ORDER BY score DESC;